﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using TP1_FlorianMetz.Class;
using TP1_FlorianMetz.Enumeration;
using Type = TP1_FlorianMetz.Enumeration.Type;

namespace TP1_FlorianMetz
{
    public class SpaceInvaders
    {
        private static List<Player> Players { get; set; }
        private static VictoryState State { get; set; }
        public SpaceInvaders()
        {
            Players = InitPlayer();
            State = VictoryState.Playing;
        }

        static void Main(string[] args)
        {
            var game = new SpaceInvaders();     //on crée une partie
            game.DisplayPlayer();               //on affiche les joueurs et les données de leur vaisseau
            var armory = new Armory();
            Console.WriteLine(armory);          //on affiche le contenu de l'armurerie
            //Players[0].Ship.AddWeapon(new Weapon("test", 0,0,Type.Direct), armory); //on test notre exception

            ChooseWeapon(armory);               //on choisi ses armes
            game.DisplayPlayer();
            Console.WriteLine("It's time to deal some damage !"); //on commence a taper
            while (State == VictoryState.Playing)
            {
                AttackRound();
                game.DisplayPlayer();
                State = TestVictory();
            }
            DisplayVictory(State);

        }

        public static void DisplayVictory(VictoryState state)
        {
            if (state == VictoryState.Draw)
            {
                Console.WriteLine("No ones win...");
            }
            else if (state == VictoryState.Victory)
            {
                var winner = Players.Where(x => !x.Ship.IsDead).FirstOrDefault();
                Console.WriteLine($"***********\tThe winner is : {winner}\t***********");
            }
        }

        public static VictoryState TestVictory()
        {
            var players = Players.Where(x => !x.Ship.IsDead).ToList();
            if (players.Count > 1)
                return VictoryState.Playing;
            return players.FirstOrDefault() == null ? VictoryState.Draw : VictoryState.Victory;
        }
        
        //Chaque joueur va attaquer l'un après l'autre le joueur de son choix
        private static void AttackRound()
        {
            var rand = new Random();
            var players = Players.Where(x => !x.Ship.IsDead).ToList();
            players.ForEach(player =>
            {
                var others = Players.Where(x => x.Pseudonym != player.Pseudonym && !x.Ship.IsDead).ToList();
                if (player.Ship.IsDead)
                    Console.WriteLine($"{player} is already dead bu he has a last chance to bring someone to hell with him !");
                Console.WriteLine($"{player}, who do you want to attack ? {DisplayPlayerListOrdered(others)} (write the number)");
                int victim;
                var test= int.TryParse(Console.ReadLine(), out victim);
                if (!test)
                    victim = 1;

                Attack(player, others[victim - 1]);
            });
        }

        //Qui inflige quels dégats a qui
        public static void Attack(Player attacker, Player victim)
        {
            Console.WriteLine($"{attacker} decide to attack {victim}");
            var damage = attacker.Ship.AverageDamage();
            victim.Ship.TakeDamage(damage);
            Console.WriteLine($"{victim} took {damage} damage.");
        }

        //Chacun choisi ses armes l'un après l'autre
        private static void ChooseWeapon(Armory armory)
        {
            Players.ForEach(player =>
            {
                Console.WriteLine($"Which weapon do you want {player.Pseudonym} ? (separate their name with spaces, and take only 3 max)");
                var answer = Console.ReadLine();
                var weapons = answer.Split(' ');
                foreach (var weaponName in weapons)
                {
                    var weapon = armory.Weapons.FirstOrDefault(x => x.Name == weaponName.ToLower());
                    if (weapon==null)
                    {
                        weapon = new Weapon(weaponName, 0, 0, Type.Direct);
                    }
                    player.Ship.AddWeapon(weapon, armory);
                }
            });
        }

        public static string DisplayPlayerListOrdered(List<Player> players)
        {
            var text = "";
            for (int i = 0; i < players.Count; i++)
            {
                text += $"\n{i + 1}. {players[i]}";
            }

            return text;
        }

        private List<Player> InitPlayer()
        {
            var res = new List<Player>();
            var p1 = new Player("meTz", "FLOrian", "Flaybe");
            var p2 = new Player("EbEr", "FannY", "Pandabrutie");
            var p3 = new Player("DubOCage", "JuLIeN", "JeanMichELsDu45");
            res.Add(p1);
            res.Add(p2);
            res.Add(p3);
            return res;
        }

        private void DisplayPlayer()
        {
            foreach (var player in Players)
            {
                Console.WriteLine("\n", player);
                Console.WriteLine(player.Ship);
                Console.WriteLine("\n");
            }
        }
    }
}
