﻿using System;
using System.Collections.Generic;
using System.Linq;
using Type = TP1_FlorianMetz.Enumeration.Type;

namespace TP1_FlorianMetz.Class
{
    public class Armory
    {

        #region Patern singleton
        //implementation thread safe du patern singleton
        private static readonly Lazy<Armory> lazy = new Lazy<Armory>(() => new Armory());

        public static Armory Instance
        {
            get { return lazy.Value; }
        }

        private Armory()
        {
            Weapons = Init();
        }
        #endregion Patern singleton

        public List<Weapon> Weapons { get; set; }

        public List<Weapon> Init()
        {
            var res = new List<Weapon>();
            res.Add(new Weapon("Laser", 2, 3, Type.Direct, 1));
            res.Add(new Weapon("Hammer", 1, 8, Type.Explosive, 1.5));
            res.Add(new Weapon("Torpedo", 3, 3, Type.Guided, 2));
            res.Add(new Weapon("MachineGun", 2, 3, Type.Direct, 1));
            res.Add(new Weapon("EMG", 1, 7, Type.Explosive, 1.5));
            res.Add(new Weapon("Missile", 4, 100, Type.Guided, 4));
            return res;
        }

        public override string ToString()
        {
            var text = "Available weapon : \n";
            foreach (var weapon in Weapons)
            {
                text += weapon + "\n";
            }
            return text;
        }

        public List<Weapon> Get5BiggestAverageDamageWeapons()
        {
            return Weapons.OrderBy(x => x.AverageDamage()).Take(5).ToList();
        }
        public List<Weapon> Get5BiggestMinDamageWeapons()
        {
            return Weapons.OrderBy(x => x.MinDamage).Take(5).ToList();
        }
    }
}