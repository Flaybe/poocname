﻿using System;
using System.Collections.Generic;
using System.Linq;
using TP1_FlorianMetz.Exceptions;

namespace TP1_FlorianMetz.Class.Ships
{
    public abstract class Ship
    {
        public int MaxStructureHealth { get; set; }
        public int MaxShieldHealth { get; set; }
        public int CurrentShield { get; set; }
        public int CurrentHealth { get; set; }
        public bool IsDead { get; set; } 
        public List<Weapon> Weapons { get; set; }

        protected Ship(int maxShieldHealth, int maxStructureHealth, int currentHealth, int currentShield, bool isDead,
            List<Weapon> weapons)
        {
            MaxShieldHealth = maxShieldHealth;
            MaxStructureHealth = maxStructureHealth;
            CurrentHealth = currentHealth;
            CurrentShield = currentShield;
            IsDead = isDead;
            Weapons = weapons;
        }

        public abstract void Attack(Ship ship);

        public override string ToString()
        {
            var text = $"The shield still have {MaxShieldHealth} hp. \nThe structure still have {MaxStructureHealth} hp. \n";
            if (IsDead) text += "The ship is destroyed. \n";
            else text += "The ship is still alive \n";
            if (Weapons==null) return text;
            text += "The ship weapons are : \n";
            Weapons.ForEach(weapon =>
            {
                text += $"- {weapon}\n";
            });
        
            return text;
        }
        protected void GenericAddWeapon(Weapon weapon, Armory armory)
        {
            try
            {
                if (!armory.Weapons.Contains(weapon))
                {
                    throw new ArmoryException($"This weapon : {weapon} does not exist in armory and won't be added to the ship :");
                }

                if (Weapons.Count>=3)
                {
                    throw new ArmoryException($"This weapon : {weapon} can't be add because your ship already has 3 weapons");
                }
                Weapons.Add(weapon);
            }
            catch (ArmoryException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //infliger des dégats au vaisseau
        public bool TakeDamage(int damage)
        {
            if (MaxShieldHealth>0)
            {
                MaxShieldHealth -= damage;
                Console.WriteLine($"{ShipType()} took {damage} shield damage");
            }
            else
            {
                MaxStructureHealth -= damage;
                Console.WriteLine($"{ShipType()} took {damage} structure damage");
            }

            if (MaxStructureHealth<=0)
            {
                Console.WriteLine($"{ShipType()} is dead");
                IsDead = true;
                return IsDead;
            }
            IsDead = false;
            return IsDead;
        }

        //Affichage des Armes du vaisseau
        public void DisplayWeapon()
        {
            if (Weapons.Count>0)
            {
                var count = 1;
                Console.WriteLine("You can use those weapon : ");
                foreach (var weapon in Weapons.Where(x => (int)x.TurnCount==1).ToList())
                {
                    Console.WriteLine($"{count}) {weapon}");
                    count++;
                }
            }
            else Console.WriteLine("Le vaisseau est équipé d'aucune arme ");
        }

        public abstract string ShipType();
    }
}