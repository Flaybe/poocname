﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace TP1_FlorianMetz.Class.Ships
{
    public class ViperMKII : Ship
    {
        public ViperMKII() : base(15, 10, 10, 15, false, new ObservableCollection<Weapon>(), "anotherShip.png")
        {
            AddWeapon("MachineGun");
            AddWeapon("EMG");
            AddWeapon("Missile");
        }

        public override void Attack(Ship ship)
        {
            DisplayWeapon();
            Console.WriteLine("Choose your weapon for this turn by typing its number");
            var test = int.TryParse(Console.ReadLine(), out var weaponNumber);
            while (!test || weaponNumber > 3 || weaponNumber < 1)
            {
                Console.WriteLine("You have to type a correct number, try again");
                test = int.TryParse(Console.ReadLine(), out weaponNumber);
            }

            var chooseWeapon = Weapons[weaponNumber - 1];
            var notChooseWeapons = Weapons.Where(x => x.Name != chooseWeapon.Name);
            foreach (var weapon in notChooseWeapons)
            {
                weapon.TurnCount -= 1;
            }
            ship.TakeDamage(chooseWeapon.Shoot());
        }

        public void AddWeapon(string weaponName)
        {
            var armory = Armory.Instance;
            var weapon = armory.Weapons.FirstOrDefault(x => x.Name == weaponName);
            GenericAddWeapon(weapon, armory);
        }

        public override string ToString()
        {
            return String.Concat($"This ship is a {ShipType()}\n", base.ToString());
        }

        public override string ShipType()
        {
            return "ViperMKII";
        }
    }
}