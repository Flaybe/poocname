﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TP1_FlorianMetz.Class.Ships
{
    public class Rocinante : Ship
    {
        public Rocinante() : base(5, 3, 3, 5, false, new List<Weapon>())
        {
            AddWeapon("Torpedo");
        }

        public override void Attack(Ship ship)
        {
            var weapon = Weapons[0];
            ship.TakeDamage(weapon.Shoot());
        }

        public void AddWeapon(string weaponName)
        {
            var armory = Armory.Instance;
            var weapon = armory.Weapons.FirstOrDefault(x => x.Name == weaponName);
            GenericAddWeapon(weapon, armory);
        }
        public override string ToString()
        {
            return String.Concat($"This ship is a {ShipType()}\n", base.ToString());
        }

        public override string ShipType()
        {
            return "Rocinante";
        }
    }
}