﻿using System;
using System.Collections.Generic;
using TP1_FlorianMetz.Interfaces;

namespace TP1_FlorianMetz.Class.Ships
{
    public class Tardis : Ship, IAptitude
    {
        public Tardis() : base(0, 1, 1, 0, false, null)
        {
        }

        public override void Attack(Ship ship)
        {
        }

        public void Use(List<Ship> ships)
        {
            var rand = new Random();
            var rValue = rand.Next(0, ships.Count);
            var sValue = ships.IndexOf(this);
            var sub = ships[rValue];
            ships[rValue] = ships[sValue];
            ships[sValue] = sub;
        }

        public override string ToString()
        {
            return String.Concat($"This ship is a {ShipType()}\n", base.ToString());
        }
        public override string ShipType()
        {
            return "Tardis";
        }
    }
}