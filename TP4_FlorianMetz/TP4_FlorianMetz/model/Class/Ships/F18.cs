﻿using System;
using System.Collections.Generic;
using TP1_FlorianMetz.Interfaces;

namespace TP1_FlorianMetz.Class.Ships
{
    public class F18 : Ship, IAptitude
    {
        public F18() : base(0, 15, 15, 0, false, null, "anotherShip.png")
        {
        }

        public override void Attack(Ship ship)
        {
        }

        public void Use(List<Ship> ships)
        {
            if (ships[1]==this && ships[0].ShipType()=="ViperMKII")
            {
                ships[0].TakeDamage(10);
                IsDead = true;
            }
        }

        public override string ToString()
        {
            return String.Concat($"This ship is a {ShipType()}\n", base.ToString());
        }

        public override string ShipType()
        {
            return "F18";
        }
    }
}