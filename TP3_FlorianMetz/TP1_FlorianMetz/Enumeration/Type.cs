﻿namespace TP1_FlorianMetz.Enumeration
{
    public enum Type
    {
        Direct,
        Explosive,
        Guided
    }
}