﻿using System;
using System.Collections.Generic;
using System.Linq;
using TP1_FlorianMetz.Exceptions;

namespace TP1_FlorianMetz.Class
{
    public class Ship
    {
        public int StructureHealth { get; set; }
        public int ShieldHealth { get; set; }
        public bool IsDead { get; set; }
        public List<Weapon> Weapons { get;  }

        //vaisseau par défaut
        public Ship() 
        {
            ShieldHealth = 0;
            StructureHealth = 40;
            IsDead = false;
            Weapons = new List<Weapon>(3);
        }

        //Vaisseau personnalisé
        public Ship(int shieldHealth, int structureHealth, List<Weapon> weapons, bool isDead = false)
        {
            ShieldHealth = shieldHealth;
            StructureHealth = structureHealth;
            Weapons = weapons;
            IsDead = isDead;
        }

        public override string ToString()
        {
            var text = $"The shield still have {ShieldHealth} hp. \nThe structure still have {StructureHealth} hp. \n";
            if (IsDead) text += "The ship is destroyed. \n";
            else text += "The ship is still alive \n";
            if (Weapons.Count <= 0) return text;
            text += "The ship weapons are : \n";
            Weapons.ForEach(weapon =>
            {
                text += $"- {weapon}\n";
            });
            return text;
        }

        public void AddWeapon(Weapon weapon, Armory armory)
        {
            try
            {
                if (!armory.Weapons.Contains(weapon))
                {
                    throw new ArmoryException($"This weapon : {weapon} does not exist in armory and won't be added to the ship :");
                }

                if (Weapons.Count>=3)
                {
                    throw new ArmoryException($"This weapon : {weapon} can't be add because your ship already has 3 weapons");
                }
                Weapons.Add(weapon);
            }
            catch (ArmoryException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RemoveWeapon(Weapon weapon, List<Weapon> weapons)
        {
            try
            {
                if (weapons.Any(x => x.Equals(weapon)))
                {
                    throw new ArmoryException();
                }
                Weapons.Remove(weapon);
            }
            catch
            {
                Console.WriteLine("This weapon does not exist in the ship!");
            }
        }

        //infliger des dégats au vaisseau
        public bool TakeDamage(int damage)
        {
            if (ShieldHealth>0)
            {
                ShieldHealth -= damage;
            }
            else
            {
                StructureHealth -= damage;
            }

            if (StructureHealth<=0)
            {
                IsDead = true;
                return IsDead;
            }
            IsDead = false;
            return IsDead;
        }

        //Affichage des Armes du vaisseau
        public void DisplayWeapon()
        {
            if (Weapons.Count>0)
            {
                Console.WriteLine("Le vaisseau est équipé des armes suivantes : ");
                foreach (var weapon in Weapons)
                {
                    Console.WriteLine(weapon);
                }
            }
            else Console.WriteLine("Le vaisseau est équipé d'aucune arme ");
        }

        //Moyenne des dégats des armes
        public int AverageDamage()
        {
            if (Weapons.Count>0)
            {
                var AverageWeaponDamage = 0;
                foreach (var weapon in Weapons)
                {
                    AverageWeaponDamage += weapon.Damage();
                }
                return AverageWeaponDamage;
            }
            Console.WriteLine("This attacker doesn't have any weapon he will always deal 0 damage");
            
            return 0;
        }

        
    }
}