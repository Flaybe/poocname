﻿using System.Collections.Generic;
using TP1_FlorianMetz.Class.Ships;

namespace TP1_FlorianMetz.Interfaces
{
    public interface IAptitude
    {
        void Use(List<Ship> ships);
    }
}