﻿using System.Collections.Generic;
using System.ComponentModel;
using TP1_FlorianMetz.Enumeration;

namespace TP1_FlorianMetz.Class
{
    public class Armory
    {
        public List<Weapon> Weapons { get; set; }

        public Armory()
        {
            Weapons = Init();
        }

        public List<Weapon> Init()
        {
            var res = new List<Weapon>();
            res.Add(new Weapon("sniper", 10, 15, Type.Direct));
            res.Add(new Weapon("rocket_launcher", 7, 11, Type.Explosive));
            res.Add(new Weapon("laser_beam", 7, 11, Type.Guided));
            return res;
        }

        public override string ToString()
        {
            var text = "Available weapon : \n";
            foreach (var weapon in Weapons)
            {
                text += weapon + "\n";
            }
            return text;
        }
    }
}