﻿

using System;
using Type = TP1_FlorianMetz.Enumeration.Type;

namespace TP1_FlorianMetz.Class
{
    public class Weapon
    {
        public string Name { get;}
        public int MinDamage { get; }
        public int MaxDamage { get; }
        public Type DamageType { get; }

        public double ReloadDuration { get; set; }

        public double TurnCount { get; set; }

        public Weapon(string name, int minDamage, int maxDamage, Type damageType, double reloadDuration )
        {
            Name = name;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            DamageType = damageType;
            ReloadDuration = reloadDuration;
            TurnCount = damageType == Type.Explosive ? reloadDuration*2 : reloadDuration; //explosif met 2 fois plus de temps a recharger
        }

        public int Damage()
        {
            var rand = new Random();
            return rand.Next(MinDamage, MaxDamage+1);
        }

        public int AverageDamage()
        {
            return (MinDamage + MaxDamage) / 2;
        }

        public int Shoot()
        {
            TurnCount -= 1;
            if (TurnCount>0)
            {
                return 0;
            }
            if (TurnCount==0)
            {
                TurnCount = DamageType == Type.Explosive ? ReloadDuration * 2 : ReloadDuration;
                if (DamageType == Type.Direct)
                {
                    var rand = new Random();
                    var value = rand.Next(0, 10); // 1 chance sur 10 de rater
                    if (value == 0) return 0;
                    return Damage();
                }
                if (DamageType == Type.Explosive)
                {
                    var rand = new Random();
                    var value = rand.Next(0, 5);
                    if (value == 0) return 0;
                    return Damage() * 2;
                }

                if (DamageType == Type.Guided)
                    return MinDamage;
            }
            return 0;
        }

        public override string ToString() => $"{Name}";

        public override bool Equals(object obj)
        {
            var weapon = obj as Weapon;
            return Name == weapon.Name && MinDamage == weapon.MinDamage && MaxDamage == weapon.MaxDamage &&
                   DamageType == weapon.DamageType;
        }
    }
}