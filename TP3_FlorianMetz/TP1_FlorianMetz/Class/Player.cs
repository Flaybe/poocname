﻿using TP1_FlorianMetz.Class.Ships;

namespace TP1_FlorianMetz.Class
{
    public class Player
    {
        private string Name { get; }
        public string Pseudonym { get; set; }
        public Ship Ship { get; }

        public Player(string lastName, string firstName, string pseudonym)
        {
            Name = $"{FormatString(firstName)} {FormatString(lastName)}";
            Pseudonym = pseudonym;
            Ship = new ViperMKII();
        }

        private static string FormatString(string s) => $"{s.Substring(0, 1).ToUpper()}{s.Substring(1).ToLower()}";


        public override string ToString()
        {
            return $"{Pseudonym} ({Name})";
        }

        public override bool Equals(object obj) => obj is Player p && Pseudonym == p.Pseudonym;
    }
}