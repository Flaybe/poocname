﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TP1_FlorianMetz.Class;
using TP1_FlorianMetz.Class.Ships;
using TP1_FlorianMetz.Enumeration;
using TP1_FlorianMetz.Interfaces;

namespace TP1_FlorianMetz
{
    public class SpaceInvaders
    {
        private static Player Player { get; set; }
        private static VictoryState State { get; set; }
        private static List<Ship> Enemies { get; set; }
        private static List<Ship> FullShips { get; set; }

        public SpaceInvaders()
        {
            Player = new Player("Metz", "Florian", "Flaybe");
            State = VictoryState.Playing;
            Enemies = Init();
            FullShips = new List<Ship>();
            FullShips.Add(Player.Ship);
            FullShips.AddRange(Enemies);
        }

        static void Main(string[] args)
        {
            var game = new SpaceInvaders();     //on crée une partie
            game.DisplayPlayer();
            game.DisplayEnemyStatus();


            Console.WriteLine("It's time to deal some damage !"); //on commence a taper
            while (State == VictoryState.Playing)
            {
                AttackRound();
                game.DisplayPlayer();
                game.DisplayEnemyStatus();
                State = TestVictory();
            }
            DisplayVictory(State);

        }

        public static void DisplayVictory(VictoryState state)
        {
            if (Player.Ship.IsDead)
            {
                Console.WriteLine("The Player lost, the enemies team won !");
            }
            else
            {
                Console.WriteLine("The Player win, well played to him !");
            }
        }

        public static VictoryState TestVictory()
        {
            var aliveNumber = Enemies.Count(x => !x.IsDead);
            if (!Player.Ship.IsDead && aliveNumber > 0)
                return VictoryState.Playing;
            return  VictoryState.Victory;
        }
        
        // Fonctionnement par tour
        private static void AttackRound()
        {
            ReloadShields();
            UseSpecialPower();
            var rand = new Random();
            var count = 0;
            var hasAttack = false;
            foreach (var enemy in Enemies)
            {
                Thread.Sleep(1000);
                var aliveCount = Enemies.Count(x => !x.IsDead);
                var value = rand.Next(0, aliveCount);

                if (value <= count && !hasAttack)
                {
                    var ship = Enemies[rand.Next(0, aliveCount)];
                    Console.WriteLine($"The player is attacking {ship.ShipType()}");
                    Player.Ship.Attack(ship);
                    hasAttack = true;
                }
                if (!enemy.IsDead)
                {
                    Console.WriteLine($"The {enemy.ShipType()} attack the player");
                    enemy.Attack(Player.Ship);
                }

                count++;
            }
        }

        private static void UseSpecialPower()
        {
            var copy = new List<Ship>();
            copy.AddRange(FullShips);
            foreach (var ship in copy)
            {
                if (ship is IAptitude newShip)
                {
                    newShip.Use(FullShips);
                }
            }
        }

        private static void ReloadShields()
        {
            var rand = new Random();
            var damageds = FullShips.Where(x => x.CurrentShield < x.MaxShieldHealth && !x.IsDead).ToList();
            foreach (var ship in damageds)
            {
                var value = rand.Next(0, 3);
                if (ship.CurrentShield + value >= ship.MaxShieldHealth)
                {
                    ship.CurrentShield = ship.MaxShieldHealth;
                }
                else
                {
                    ship.CurrentShield += value;
                }
            }
        }

        private List<Ship> Init()
        {
            var res = new List<Ship>();
            res.Add(new Dart());
            res.Add(new BWings());
            res.Add(new Rocinante());
            res.Add(new F18());
            res.Add(new Tardis());
            return res;
        }

        private void DisplayPlayer()
        {
            Thread.Sleep(1000);
            Console.WriteLine($"\n{Player}");
            Console.WriteLine(Player.Ship);
            Console.WriteLine("\n");
        }

        private void DisplayEnemyStatus()
        {
            foreach (var enemy in FullShips.Where(x => x.ShipType()!="ViperMKII"))
            {
                Thread.Sleep(1000);
                Console.WriteLine($"{enemy}");
            }
        }
    }
}
