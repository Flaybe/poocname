﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Xml;
using TP1_FlorianMetz.Class;
using TP1_FlorianMetz.Class.Ships;
using TP1_FlorianMetz.Enumeration;
using TP1_FlorianMetz.Interfaces;

namespace TP1_FlorianMetz
{
    public class SpaceInvaders
    {
        private  Player Player { get; set; }
        public  List<Player> Players { get; set; }
        private  VictoryState State { get; set; }
        private  List<Ship> Enemies { get; set; }
        private  List<Ship> FullShips { get; set; }

        public SpaceInvaders()
        {
            Player = new Player("Metz", "Florian", "Flaybe");
            Players = new List<Player>();
            Players.Add(Player);
            State = VictoryState.Playing;
            Enemies = Init();
            FullShips = new List<Ship>();
            FullShips.Add(Player.Ship);
            FullShips.AddRange(Enemies);
        }

        static void Main(string[] args, string path)
        {
            var weaponsImporter = new WeaponImporter(path, 2, new List<string>() {"test"});
            Armory.Instance.Get5BiggestAverageDamageWeapons();
            Armory.Instance.Get5BiggestMinDamageWeapons();
        }

        static void Main(string[] args)
        {
            MainMenu(new SpaceInvaders());
        }

        private static void LaunchGame(SpaceInvaders game)
        {
            game.DisplayPlayer();
            game.DisplayEnemyStatus();

            Console.WriteLine("It's time to deal some damage !"); //on commence a taper
            while (game.State == VictoryState.Playing)
            {
                AttackRound(game);
                game.DisplayPlayer();
                game.DisplayEnemyStatus();
                game.State = TestVictory(game);
            }
            DisplayVictory(game, game.State);
        }

        private static void MainMenu(SpaceInvaders game)
        {
            Console.WriteLine("-------------------------YOU ARE IN THE MAIN MENU-------------------------");
            Console.WriteLine("What do you want to do ?");
            Console.WriteLine("1) Launch the game");
            Console.WriteLine("2) Manage Player");
            Console.WriteLine("3) Manage your Ship");
            Console.WriteLine("4) Manage the armory");

            var choice = TestInt(1, 4);
            switch (choice)
            {
                case 1:
                    Console.Clear();
                    LaunchGame(game);
                    break;
                case 2:
                    Console.Clear();
                    PlayerManagingMenu(game);
                    break;
                case 3:
                    Console.Clear();
                    ShipManagingMenu(game);
                    break;
                case 4:
                    Console.Clear();
                    ArmoryManagingMenu(game);
                    break;
            }
        }

        private static void PlayerManagingMenu(SpaceInvaders game)
        {
            Console.WriteLine("-------------------------YOU ARE IN THE PLAYER MENU-------------------------");
            Console.WriteLine("What do you want to do ?");
            Console.WriteLine("1) Add a Player");
            Console.WriteLine("2) Select another player");
            Console.WriteLine("3) Delete a Player");
            Console.WriteLine("4) Go Back");

            var choice = TestInt(1, 4);
            switch (choice)
            {
                case 1:
                    Console.Clear();
                    CreatePlayer(game);
                    break;
                case 2:
                    Console.Clear();
                    SelectPlayer(game);
                    break;
                case 3:
                    Console.Clear();
                    DeletePlayer(game);
                    break;
                case 4:
                    Console.Clear();
                    MainMenu(game);
                    break;
            }
        }

        private static void CreatePlayer(SpaceInvaders game)
        {

            Console.WriteLine("Write down his name");
            var name = Console.ReadLine();
            Console.WriteLine("Write down his first name");
            var firstName = Console.ReadLine();
            Console.WriteLine("Write down his pseudo");
            var pseudo = Console.ReadLine();
            game.Players.Add(new Player(name, firstName, pseudo));
            Console.Clear();
            PlayerManagingMenu(game);
        }

        private static int ListPlayer(SpaceInvaders game)
        {
            var count = 1;
            foreach (var player in game.Players)
            {
                Console.WriteLine($"{count}) {player.Pseudonym}");
                count++;
            }
            return TestInt(1, count); ;
        }

        private static void SelectPlayer(SpaceInvaders game)
        {
            Console.WriteLine("Who do you want to choose ?");
            var choice = ListPlayer(game);
            game.Player = game.Players[choice - 1];
            Console.Clear();
            PlayerManagingMenu(game);
        }

        private static void DeletePlayer(SpaceInvaders game)
        {
            Console.WriteLine("Who do you want to delete ?");
            var choice = ListPlayer(game);
            game.Players.RemoveAt(choice-1);
            Console.Clear();
            PlayerManagingMenu(game);
        }

        private static void ShipManagingMenu(SpaceInvaders game)
        {
            Console.WriteLine("-------------------------YOU ARE IN THE SHIP MENU-------------------------");
            Console.WriteLine("What do you want to do ?");
            Console.WriteLine("1) Add a Weapon");
            Console.WriteLine("2) Display your weapon");
            Console.WriteLine("3) Remove a Weapon");
            Console.WriteLine("4) Go Back");

            var choice = TestInt(1, 4);
            switch (choice)
            {
                case 1:
                    Console.Clear();
                    AddShipWeapon(game);
                    break;
                case 2:
                    Console.Clear();
                    DisplayShipWeapon(game);
                    break;
                case 3:
                    Console.Clear();
                    RemoveShipWeapon(game);
                    break;
                case 4:
                    Console.Clear();
                    MainMenu(game);
                    break;
            }
        }

        private static int ListWeapons(List<Weapon> weapons)
        {
            var count = 1;
            foreach (var weapon in weapons)
            {
                Console.WriteLine($"{count}) {weapon}");
                count++;
            }
            return TestInt(1, count); ;
        }

        private static void AddShipWeapon(SpaceInvaders game)
        {
            if (game.Player.Ship.Weapons.Count>3)
            {
                Console.WriteLine("You already have the maximum number of weapon on your ship, remove another one first");
                ShipManagingMenu(game);
            }
            Console.WriteLine("Which weapon do you want to add ?");
            var choice =  ListWeapons(Armory.Instance.Weapons);
            game.Player.Ship.Weapons.Add(Armory.Instance.Weapons[choice-1]);
            ShipManagingMenu(game);
        }

        private static void DisplayShipWeapon(SpaceInvaders game)
        {
            Console.WriteLine("Your weapons are :");
            var count = 1;
            foreach (var weapon in game.Player.Ship.Weapons)
            {
                Console.WriteLine($"{count}) {weapon}");
                count++;
            }
            Console.WriteLine("Press enter to go back");
            Console.ReadLine();
            ShipManagingMenu(game);
        }

        private static void RemoveShipWeapon(SpaceInvaders game)
        {
            if (game.Player.Ship.Weapons.Count <=0)
            {
                Console.WriteLine("You don't have any weapon, add one first");
                ShipManagingMenu(game);
            }
            Console.WriteLine("Which weapon do you want to remove ?");
            var choice = ListWeapons(game.Player.Ship.Weapons);
            game.Player.Ship.Weapons.RemoveAt(choice-1);
            ShipManagingMenu(game);
        }

        private static void ArmoryManagingMenu(SpaceInvaders game)
        {
            Console.WriteLine("-------------------------YOU ARE IN THE ARMORY MENU-------------------------");
            Console.WriteLine("What do you want to do ?");
            Console.WriteLine("1) Create a Weapon");
            Console.WriteLine("2) Display Armory's weapons");
            Console.WriteLine("3) Remove a Weapon");
            Console.WriteLine("4) Go Back");

            var choice = TestInt(1, 4);
            switch (choice)
            {
                case 1:
                    Console.Clear();
                    CreateArmoryWeapon(game);
                    break;
                case 2:
                    Console.Clear();
                    DisplayArmoryWeapon(game);
                    break;
                case 3:
                    Console.Clear();
                    RemoveArmoryWeapon(game);
                    break;
                case 4:
                    Console.Clear();
                    MainMenu(game);
                    break;
            }
        }

        private static void CreateArmoryWeapon(SpaceInvaders game)
        {
            Console.WriteLine("-------------------------YOU ARE IN THE PLAYER MENU-------------------------");

            Console.WriteLine("Write down his name");
            var name = Console.ReadLine();
            Console.WriteLine("Write down his minDamage (between 1 and 5)");
            var minDamage = TestInt(1, 5);
            Console.WriteLine("Write down his maxDamage (between 5 and 10)");
            var maxDamage = TestInt(5, 10);
            Console.WriteLine("Write down his reloadDuration (between 1 and 3)");
            var reloadDuration = TestInt(1,3);
            Console.WriteLine("Write down his DamageType \n1) direct damage,\n2) explosive damage,\n3) guided damage ");
            var type = (Enumeration.Type)TestInt(1, 3)-1;
            Armory.Instance.Weapons.Add(new Weapon(name, minDamage, maxDamage, type, reloadDuration));

            ArmoryManagingMenu(game);
        }

        private static void DisplayArmoryWeapon(SpaceInvaders game)
        {
            Console.WriteLine("Armory weapons are :");
            var count = 1;
            foreach (var weapon in Armory.Instance.Weapons)
            {
                Console.WriteLine($"{count}) {weapon}");
                count++;
            }
            Console.WriteLine("Press enter to go back");
            Console.ReadLine();
            ArmoryManagingMenu(game);
        }

        private static void RemoveArmoryWeapon(SpaceInvaders game)
        {
            if (Armory.Instance.Weapons.Count <= 0)
            {
                Console.WriteLine("There isn't any weapon, add one first");
                ArmoryManagingMenu(game);
            }
            Console.WriteLine("Which weapon do you want to remove ?");
            var choice = ListWeapons(Armory.Instance.Weapons);
            Armory.Instance.Weapons.RemoveAt(choice - 1);
            ArmoryManagingMenu(game);

        }

        public static int TestInt(int minValue, int maxValue)
        {
            var test = int.TryParse(Console.ReadLine(), out var value);
            while (!test || value > maxValue || value < minValue)
            {
                Console.WriteLine("You have to type a correct number, try again");
                test = int.TryParse(Console.ReadLine(), out value);
            }

            return value;
        }


        public static void DisplayVictory(SpaceInvaders game, VictoryState state)
        {
            if (game.Player.Ship.IsDead)
            {
                Console.WriteLine("The Player lost, the enemies team won !");
            }
            else
            {
                Console.WriteLine("The Player win, well played to him !");
            }
        }

        public static VictoryState TestVictory(SpaceInvaders game)
        {
            var aliveNumber = game.Enemies.Count(x => !x.IsDead);
            if (!game.Player.Ship.IsDead && aliveNumber > 0)
                return VictoryState.Playing;
            return  VictoryState.Victory;
        }
        
        // Fonctionnement par tour
        private static void AttackRound(SpaceInvaders game)
        {
            ReloadShields(game);
            UseSpecialPower(game);
            var rand = new Random();
            var count = 0;
            var hasAttack = false;
            foreach (var enemy in game.Enemies)
            {
                Thread.Sleep(1000);
                var aliveCount = game.Enemies.Count(x => !x.IsDead);
                var value = rand.Next(0, aliveCount);

                if (value <= count && !hasAttack)
                {
                    var ship = game.Enemies[rand.Next(0, aliveCount)];
                    Console.WriteLine($"The player is attacking {ship.ShipType()}");
                    game.Player.Ship.Attack(ship);
                    hasAttack = true;
                }
                if (!enemy.IsDead)
                {
                    Console.WriteLine($"The {enemy.ShipType()} attack the player");
                    enemy.Attack(game.Player.Ship);
                }

                count++;
            }
        }

        private static void UseSpecialPower(SpaceInvaders game)
        {
            var copy = new List<Ship>();
            copy.AddRange(game.FullShips);
            foreach (var ship in copy)
            {
                if (ship is IAptitude newShip)
                {
                    newShip.Use(game.FullShips);
                }
            }
        }

        private static void ReloadShields(SpaceInvaders game)
        {
            var rand = new Random();
            var damageds = game.FullShips.Where(x => x.CurrentShield < x.MaxShieldHealth && !x.IsDead).ToList();
            foreach (var ship in damageds)
            {
                var value = rand.Next(0, 3);
                if (ship.CurrentShield + value >= ship.MaxShieldHealth)
                {
                    ship.CurrentShield = ship.MaxShieldHealth;
                }
                else
                {
                    ship.CurrentShield += value;
                }
            }
        }

        private List<Ship> Init()
        {
            var res = new List<Ship>();
            res.Add(new Dart());
            res.Add(new BWings());
            res.Add(new Rocinante());
            res.Add(new F18());
            res.Add(new Tardis());
            return res;
        }

        private void DisplayPlayer()
        {
            Thread.Sleep(1000);
            Console.WriteLine($"\n{Player}");
            Console.WriteLine(Player.Ship);
            Console.WriteLine("\n");
        }

        private void DisplayEnemyStatus()
        {
            foreach (var enemy in FullShips.Where(x => x.ShipType()!="ViperMKII"))
            {
                Thread.Sleep(1000);
                Console.WriteLine($"{enemy}");
            }
        }
    }
}
