﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using TP1_FlorianMetz.Class;

namespace TP4_FlorianMetz.viewmodel
{
    public class MainViewModel
    {
        public ObservableCollection<Player> players;

        public void InitPlayer()
        {
            players = new ObservableCollection<Player>();
            players.Add(new Player("Metz", "Florian", "Flaybe"));
            players.Add(new Player("Eber", "Fanny", "Pandabrutie"));
        }

        public string GenerateWeapon()
        {
            var armory = Armory.Instance;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                WeaponImporter.GenerateWeapon(openFileDialog.FileName);
            }

            return armory.CurrentWeapons();
        }

        public BitmapImage LoadImage()
        {
            var op = new OpenFileDialog
            {
                Title = "Select a picture",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                         "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                         "Portable Network Graphic (*.png)|*.png"
            };
            if (op.ShowDialog() == true)
            {
               return new BitmapImage(new Uri(op.FileName));
            }

            return null;
        }

        public void SaveImage(BitmapImage image, string filePath)
        {
            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));

            using (var fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
            {
                encoder.Save(fileStream);
            }
        }
    }
}
