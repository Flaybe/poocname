﻿using TP1_FlorianMetz.Class;

namespace TP1_FlorianMetz.Exceptions
{
    public class ArmoryException : System.Exception
    {
        public Weapon Weapon { get; }

        public ArmoryException() { }

        public ArmoryException(string message)
            : base(message) { }

        public ArmoryException(string message, System.Exception inner)
            : base(message, inner) { }
        
        public ArmoryException(string message, Weapon weapon)
            : this(message)
        {
            Weapon = weapon;
        }
    }
}