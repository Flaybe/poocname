﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TP1_FlorianMetz.Exceptions;

namespace TP1_FlorianMetz.Class.Ships
{
    public abstract class Ship
    {
        public int MaxStructureHealth { get; set; }
        public int MaxShieldHealth { get; set; }
        public int CurrentShield { get; set; }
        public int CurrentHealth { get; set; }
        public bool IsDead { get; set; } 
        public ObservableCollection<Weapon> Weapons { get; set; }
        public string ImgPath { get; set; }

        protected Ship(int maxShieldHealth, int maxStructureHealth, int currentHealth, int currentShield, bool isDead,
            ObservableCollection<Weapon> weapons, string url = null)
        {
            MaxShieldHealth = maxShieldHealth;
            MaxStructureHealth = maxStructureHealth;
            CurrentHealth = currentHealth;
            CurrentShield = currentShield;
            IsDead = isDead;
            Weapons = weapons;
            ImgPath = url ?? "defaultShip.png";
        }

        public abstract void Attack(Ship ship);

        public override string ToString()
        {
            return $"The shield still have {MaxShieldHealth} hp. \nThe structure still have {MaxStructureHealth} hp. \n";
        }
        protected void GenericAddWeapon(Weapon weapon, Armory armory)
        {
            try
            {
                if (!armory.Weapons.Contains(weapon))
                {
                    throw new ArmoryException($"This weapon : {weapon} does not exist in armory and won't be added to the ship :");
                }

                if (Weapons.Count>=3)
                {
                    throw new ArmoryException($"This weapon : {weapon} can't be add because your ship already has 3 weapons");
                }
                Weapons.Add(weapon);
            }
            catch (ArmoryException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //infliger des dégats au vaisseau
        public bool TakeDamage(int damage)
        {
            if (MaxShieldHealth>0)
            {
                MaxShieldHealth -= damage;
                Console.WriteLine($"{ShipType()} took {damage} shield damage");
            }
            else
            {
                MaxStructureHealth -= damage;
                Console.WriteLine($"{ShipType()} took {damage} structure damage");
            }

            if (MaxStructureHealth<=0)
            {
                Console.WriteLine($"{ShipType()} is dead");
                IsDead = true;
                return IsDead;
            }
            IsDead = false;
            return IsDead;
        }

        //Affichage des Armes du vaisseau
        public void DisplayWeapon()
        {
            if (Weapons.Count>0)
            {
                var count = 1;
                Console.WriteLine("You can use those weapon : ");
                foreach (var weapon in Weapons.Where(x => (int)x.TurnCount==1).ToList())
                {
                    Console.WriteLine($"{count}) {weapon}");
                    count++;
                }
            }
            else Console.WriteLine("Le vaisseau est équipé d'aucune arme ");
        }

        public abstract string ShipType();
    }
}