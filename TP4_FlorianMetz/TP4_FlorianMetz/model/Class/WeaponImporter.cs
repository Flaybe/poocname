﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TP1_FlorianMetz.Enumeration;

namespace TP1_FlorianMetz.Class
{
    public class WeaponImporter
    {
        public Dictionary<string, int> WordFrequency { get; set; }
        public string ReadedFile { get; set; }
        public static int MinWordSize { get; set; }
        public static List<string> BlackList { get; set; }

        public WeaponImporter(string file, int minWordSize, List<string> blackList)
        {
            MinWordSize = minWordSize;
            BlackList = blackList;
            ReadedFile = file;
            WordFrequency = GetWordFrequency(file);
        }

        public WeaponImporter(){}

        public static Dictionary<string, int> GetWordFrequency(string file)
        {
            return File.ReadLines(file)
                        .SelectMany(x => x.Split())                             //on divise le texte par mot
                        .Where(x => WordValidation(x))  //on prend que les mots valide
                        .GroupBy(x => x)                                        //on groupe par mot
                        .ToDictionary(x => Normalize(x.Key), x => x.Count());
        }               //on crée un dictionnaire avec le mot comme clé et son nombre d'itération comme frequence

        private static bool WordValidation(string word, int minWordSize=3, List<string> blackList=null)
        {
            if (word.Length < minWordSize || (blackList != null && blackList.Contains(word)))
                return false;
            return true;
        }

        private static string Normalize(string text)
        {
            var textInfo = new CultureInfo("en-US", false).TextInfo;
            return Regex.Replace(textInfo.ToTitleCase(text), "[?.!:;,]", "");
        }

        public static void GenerateWeapon(string file)
        {
            var armory = Armory.Instance;
            foreach (var item in GetWordFrequency(file))
            {
                var wordLength = item.Key.Length;
                var frequency = item.Value;
                var minDamage = frequency;
                var maxDamage = wordLength;
                if (wordLength < frequency)
                {
                    minDamage = wordLength;
                    maxDamage = frequency;
                }

                var rand = new Random();
                var weapon = new Weapon(item.Key, minDamage, maxDamage, (Enumeration.Type)rand.Next(0, 3), rand.Next(0, 4));
                armory.Weapons.Add(weapon);
            }
        }
    }
}