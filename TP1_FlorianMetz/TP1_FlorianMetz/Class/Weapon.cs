﻿

using System;
using Type = TP1_FlorianMetz.Enumeration.Type;

namespace TP1_FlorianMetz.Class
{
    public class Weapon
    {
        public string Name { get;}
        public int MinDamage { get; }
        public int MaxDamage { get; }
        public Type DamageType { get; }

        public Weapon(string name, int minDamage, int maxDamage, Type damageType )
        {
            Name = name;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            DamageType = damageType;
        }

        public int Damage()
        {
            var rand = new Random();
            return rand.Next(MinDamage, MaxDamage);
        }

        public override string ToString() => $"{Name}";

        public override bool Equals(object obj)
        {
            var weapon = obj as Weapon;
            return Name == weapon.Name && MinDamage == weapon.MinDamage && MaxDamage == weapon.MaxDamage &&
                   DamageType == weapon.DamageType;
        }
    }
}