﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using TP1_FlorianMetz.Class;
using TP4_FlorianMetz.viewmodel;

namespace TP4_FlorianMetz
{
    public partial class MainWindow : Window
    {
        public MainViewModel _vm;

        public MainWindow()
        {
            _vm = new MainViewModel();
            InitializeComponent();
            initPlayer();
            Armory.Instance.Init();
            dgArmory.ItemsSource = Armory.Instance.Weapons;
        }

        private void initPlayer()
        {
            _vm.InitPlayer();
            RefreshListBox();
        }

        private void BtnOpenFile_OnClick(object sender, RoutedEventArgs e)
        {
            armoryChosenText.Text = _vm.GenerateWeapon();
        }

        private void BtnLoad_OnClick(object sender, RoutedEventArgs e)
        {
            if (PlayerLsBx?.SelectedItems == null) return;
            var source = _vm.LoadImage();
            imgShip.Source = source ?? new BitmapImage(new Uri("../images/chooseShip.png"));
            var player = (Player)PlayerLsBx.SelectedItems[0];
            player.Ship.ImgPath = source.UriSource.Segments.Last();
            _vm.SaveImage(source, $"../../images/{source.UriSource.Segments.Last()}");
        }


        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            _vm.players.Add(new Player("nouveau", "joueur", $"{_vm.players.Count}"));
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            PlayerLsBx.Items.Clear();
            foreach (var vmPlayer in _vm.players) PlayerLsBx.Items.Add(vmPlayer);
        }

        private void RmButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (PlayerLsBx?.SelectedItems != null)
            {
                foreach (var selectedItem in PlayerLsBx.SelectedItems) _vm.players.Remove((Player)selectedItem);
                RefreshListBox();
            }
        }

        private void PlayerLsBx_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnLoad.Visibility = Visibility.Visible;
            var player = (Player)PlayerLsBx.SelectedItems[0];
            imgShip.Source = new BitmapImage(new Uri($"../../images/{player?.Ship?.ImgPath}", UriKind.Relative));
            lblName.Content = $"Name : {player?.Name}";
            lblPseudo.Content = $"Pseudonym : {player?.Pseudonym}";
            lblShip.Content = $"Ship : {player?.Ship}";
            dgWeapons.ItemsSource = player?.Ship?.Weapons;
        }

        private void RmWeapBtn_OnClick(object sender, RoutedEventArgs e)
        {
            if (dgWeapons.SelectedItems == null) return;
            var player = (Player)PlayerLsBx.SelectedItems[0];
            player.Ship.Weapons.Remove((Weapon)dgWeapons.SelectedItem);
        }

        private void ShowWeapBtn_OnClick(object sender, RoutedEventArgs e)
        {
            ShowAddFromArmory(true);
        }

        private void AddSelectedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var player = (Player)PlayerLsBx.SelectedItems[0];
            foreach (var dgArmorySelectedItem in dgArmory.SelectedItems)
            {
                if (player.Ship.Weapons.Count >= 3)
                {
                    WarningLabel.Visibility = Visibility.Visible;
                    return;
                }

                player.Ship.Weapons.Add((Weapon)dgArmorySelectedItem);
            }

            ShowAddFromArmory(false);
        }

        public void ShowAddFromArmory(bool show)
        {
            if (show)
            {
                ArmoryGrpBox.Visibility = Visibility.Visible;
                AddSelectedBtn.Visibility = Visibility.Visible;
            }
            else
            {
                ArmoryGrpBox.Visibility = Visibility.Hidden;
                AddSelectedBtn.Visibility = Visibility.Hidden;
            }
        }
    }
}