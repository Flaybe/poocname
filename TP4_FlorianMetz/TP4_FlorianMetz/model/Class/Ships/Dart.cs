﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Type = TP1_FlorianMetz.Enumeration.Type;

namespace TP1_FlorianMetz.Class.Ships
{
    public class Dart : Ship
    {
        public Dart() : base(3, 10, 10, 3, false, new ObservableCollection<Weapon>(), "anotherShip.png")
        {
            AddWeapon("Laser");
        }
        
        public override void Attack(Ship ship)
        {
            var weapon = Weapons[0];
            ship.TakeDamage(weapon.Shoot());
        }

        public void AddWeapon(string weaponName)
        {
            var armory = Armory.Instance;
            var weapon = armory.Weapons.FirstOrDefault(x => x.Name == weaponName);
            if (weapon!=null && weapon.DamageType == Type.Direct)
            {
                weapon.ReloadDuration = 1;
                weapon.TurnCount = 1;
            }
            GenericAddWeapon(weapon, armory);
        }

        public override string ToString()
        {
            return String.Concat($"This ship is a {ShipType()}\n", base.ToString());
        }

        public override string ShipType()
        {
            return "Dart";
        }
    }
}